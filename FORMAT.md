# dontreadthecomments filter format definition

Because `dontreadthecomments` intends to be able to generate filters for both
adblockes and for use with generic CSS filters, it has its own, rather simple,
file format for defining rules. At its core it is a [YAML](https://yaml.org/)
file, so a basic understanding of how YAML works is neccessary to understand
the rest of this definition.

Note that anywhere it expects a YAML array, you may omit the array if it has
only one element.

## Root elements

The toplevel/root of the YAML file is a set of YAML keys. Each key is the
"name" of the filter. This name is only used to reference the filter, and can
be anything that is legal in YAML, *but* it **must** be unique, and not just
within a single file - it must be unique among all of the filter files in
`dontreadthecomments`. The keys underneath define the filter itself.

## Filter keys

All keys are optional, and may be omitted. They may also be combined in any
way, unless otherwise specified.

### cssRules

This is a list (YAML array) of CSS selectors that you want to hide. Any
elements that match these CSS selectors will have "display:none;
visibility:hidden;" applied to them (in the CSS variant) or be labelled for
blocking (in the adblock variant).

### domain

This sets which domain the filters wil apply to. You may omit this to apply the
filter everywhere to all pages.

### requiresDomain

If this is set to `true`, then the rules will be ignored for the CSS-filter
file, since this file can't handle the `domain` setting. If this is **not** set
to `true`, then the CSS filters will act as if the `domain` setting wasn't
there (while the adblock filters will continue to use it as normal).

### adblockRules

This is a list (YAML array) of [AdBlock plus](https://adblockplus.org/en/filters)
syntax filters. You should only use these if you need to block requests. If you
are filtering using CSS selectors, use cssRules instead.


### cssOnlyRules

This works like `cssRules`, but the adblock filter will not include them. This
can be useful where you have a `adblockRules` that blocks requests, but also
want to enable filtering in CSS, which can't block requests.

### onlyAdblock

If set to true, specifies that the filter is only valid for adblock. It will
not be output as a CSS filter.

### skipAdblock

If set to true, specifies that the filter is not valid for adblock. It will be
skipped when generating adblock-format filter files.
