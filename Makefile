default: build
test: _sanity build
distbuild: _sanity build md2html
clean:
	rm -rf built/ public/
_buildPrep:
	mkdir -p built/

MARKDOWN_BUILDER=perl -MText::Markdown -MIO::All -E 'my $$full = !$$ENV{DRTC_BASEHTML};say "<!DOCTYPE html><html><head><title>dontreadthecomments</title></head><body>" if $$full; say Text::Markdown::markdown( scalar io(shift(@ARGV))->slurp ); say "</body></html>" if $$full'
HTML_EXT?=.html
HTML_LINKPART?=.html
md2html: _buildPrep
	mkdir -p built/docs/img
	$(MARKDOWN_BUILDER) README.md > built/index$(HTML_EXT)
	$(MARKDOWN_BUILDER) FORMAT.md > built/FORMAT$(HTML_EXT)
	for f in docs/*md; do out="$$(echo "$$f"|perl -p -E 's/\.md/$(HTML_EXT)/g')"; $(MARKDOWN_BUILDER) "$$f" > built/"$$out";done
	perl -pi -E 's/\.md/$(HTML_LINKPART)/g' built/*$(HTML_EXT) built/docs/*$(HTML_EXT)
	cp docs/img/* built/docs/img/

_sanity:
	perl -c build.pl
build: _buildPrep
	@perl build.pl
