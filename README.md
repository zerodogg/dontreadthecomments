# Don't read the comments
## …and don't feed the trolls

[![Youtube music video link](http://img.youtube.com/vi/VyugfHPOFBE/0.jpg)](http://www.youtube.com/watch?v=VyugfHPOFBE "Jonathan Coulton - Don't feed the trolls")

Comment sections everywhere are riddled by hateful and sometimes downright
nasty people. In many places they now require a person's full name to allow
them to comment - but that doesn't appear to have made any difference. Many of
these people are a lost cause, refusing to see any kind of reason. Reading
these comments can be downright unhealthy, and trying to engage them appears to
be futile. Thus, the solution, to maintain one's mental health, is to block
comments.

# How to use the filter

To use the filter you will need an adblocking extension installed in your
browser. If you don't have one, install uBlock origin for your browser:
[Firefox](https://addons.mozilla.org/addon/ublock-origin/) or
[Chromium/Google Chrome](https://chrome.google.com/webstore/detail/ublock-origin/cjpalhdlnbpafiamejdnhcphjbkeiagm).

Once you have an adblocker installed, [follow this link to enable
dontreadthecomments](abp:subscribe?location=https%3A//zerodogg.gitlab.io/dontreadthecomments/adblock.txt&title=dontreadthecomments%20-%20comment%20filter).

If that link for some reason does not work for you, see the manual instructions
for adding dontreadthecomments to [uBlock origin](docs/uBlockOrigin.md) or [any
other adblocker](docs/genericAdblock.md).

## Using the filter without an adblocker

The dontreadthecomments filter is also available as a custom CSS ruleset. This
can be applied in browsers that doesn't support AdBlock-style extensions, but
that does support custom CSS rules. You should only use this if you are certain
that your browser doesn't support an AdBlock extension, for instance if you are
using a browser like [qutebrowser](https://qutebrowser.org).

See the [instructions for using the CSS filters](docs/genericCSS.md).

# How to contribute filters

Contributions are most welcome. If you know a bit of CSS, you can write filters
yourself and send pull requests. See the [FORMAT.md](FORMAT.md) file for information on the
syntax. If you don't know how, then you can submit a issue report requesting a
filter to be written. Most websites will be accepted, the exception is websites
where the comment aspect is obviously the main point of the site, such as
facebook, twitter and reddit.

# License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
