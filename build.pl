#!/usr/bin/perl
# dontreadthecoments filter builder
# Copyright (C) Eskild Hustvedt 2019
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
use strict;
use warnings;
use 5.020;
use YAML::XS;
use IO::All;
use List::Util 1.45 qw(uniqstr);

# This function converts whatever it is provided with to an array.
# An array ref will be dereferenced, a single element will be returned.
sub expectsArray
{
    my $thing = shift;
    if (!defined($thing))
    {
        return ();
    }
    if(ref($thing))
    {
        return @{$thing};
    }
    return $thing;
}

# Loads a set of rule files, merges them and returns a hashref containing
# all of them
sub loadRuleFiles
{
    my @files = @_;
    my %rules;
    foreach my $file (@files)
    {
        my $content = Load( scalar io($file)->slurp );
        foreach my $rule (keys %{$content})
        {
            $content->{$rule}->{_meta} = {
                from => $file,
                name => $rule
            };
            $rules{ $file.'-'.$rule } = $content->{$rule};
        }
    }
    return \%rules;
}

# Generates a CSS filter file
sub generateGenericCSS
{
    my $rules = shift;
    my @cssRules;
    # Header
    my $header = '/*
dontreadthecomments - comment filter
Filters that intends to remove comments from websites, improving mental health everywhere

https://gitlab.com/zerodogg/dontreadthecomments

This file is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This file is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this file.  If not, see <http://www.gnu.org/licenses/>
*/
';
    foreach my $ruleKey (sort keys %{$rules})
    {
        my $rule = $rules->{$ruleKey};
        if ($rule->{requiresDomain} || $rule->{onlyAdblock})
        {
            next;
        }
        if ($rule->{cssRules})
        {
            push(@cssRules,join(',',expectsArray($rule->{cssRules})));
        }
        if ($rule->{cssOnlyRules})
        {
            push(@cssRules,join(',',expectsArray($rule->{cssOnlyRules})));
        }
    }
    my $str = $header;
    $str .= join(',',sort(uniqstr(@cssRules)))."\n"."{"."display:none !important; visibility:hidden !important;"."}";
    return $str;
}

# Generates a adblock-format filter filer
sub generateAdblock
{
    my $rules = shift;
    my @adbRules;
    # Add our header
    push(@adbRules,'! Title: dontreadthecomments - comment filter
! Expires: 8 hours
! Description: Block web comments - improve mental health and recover your belief in humanity
! Homepage: https://gitlab.com/zerodogg/dontreadthecomments
! Licence: https://gitlab.com/zerodogg/dontreadthecomments/blob/master/COPYING
!
! GitLab issues: https://gitlab.com/zerodogg/dontreadthecomments/issues
! GitLab merge requests: https://gitlab.com/zerodogg/dontreadthecomments/merge_requests
');
    foreach my $ruleKey (sort keys %{$rules})
    {
        my $rule = $rules->{$ruleKey};
        if ($rule->{skipAdblock})
        {
            next;
        }
        my @domains;
        push(@domains,expectsArray($rule->{domain}));
        push(@adbRules,'! '.$rule->{_meta}->{name}.' from '.$rule->{_meta}->{from});
        foreach my $cssRule (expectsArray($rule->{cssRules}))
        {
            push(@adbRules, join(',',@domains).'##'.$cssRule);
        }
        foreach my $adbRule (expectsArray($rule->{adblockRules}))
        {
            push(@adbRules, join(',',@domains).$adbRule);
        }
    }
    my $str = join("\n",@adbRules);
    return $str;
}

# Main entry point
sub main
{
    my $rules = loadRuleFiles(glob('filters/*yaml'));

    my $generic = generateGenericCSS($rules);
    io('built/generic.css')->print($generic);
    say 'Wrote built/generic.css';

    my $adblock = generateAdblock($rules);
    io('built/adblock.txt')->print($adblock);
    say 'Wrote built/adblock.txt';
}

main();
