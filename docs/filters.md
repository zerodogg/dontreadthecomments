# Filters

At the moment there is only one, big, generic filter list. In the future there
will be country-specific filters, but as of right now there aren't enough
filters to warrant this.

## Generic

- [Add to uBlock Origin, Adblock Plus and similar adblockers](abp:subscribe?location=https%3A//zerodogg.gitlab.io/dontreadthecomments/adblock.txt&title=dontreadthecomments%20-%20comment%20filter)
- Adblock file (to manually add it): [https://zerodogg.gitlab.io/dontreadthecomments/adblock.txt](https://zerodogg.gitlab.io/dontreadthecomments/adblock.txt)
- Generic CSS file: [https://zerodogg.gitlab.io/dontreadthecomments/generic.css](https://zerodogg.gitlab.io/dontreadthecomments/generic.css)
