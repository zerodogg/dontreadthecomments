# Using dontreadthecomments filters with any adblocker

## 1 - Choose your filter

[Select a filter](filters.md) from the filter list and copy the "Adblock file"
filter link. You may wish to try the "Add to uBlock Origin, Adblock Plus and
similar adblockers" link before proceeding to the next step, if that link works
you do not need to follow step 2.

## 2 - Adding the filter to the adblocker

1. Find the settings for your adblocker, usually by clicking the icon for the
adblocker in your browser, or through your browser's extensions screen.
2. Find the list of filters, usually called "Filter subscriptions" or ""Filter
   lists"
3. Add the filter. Usually there is a "add filter", "add subcription" or
   "custom" button or link. This is where you paste the URL you copied from the
   filter list in the first step.
