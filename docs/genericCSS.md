# Using dontreadthecomments filters by applying custom CSS

## 1 - Choose your filter

[Select a filter](filters.md) from the filter list and copy the "CSS"
filter link.

## 2 - Adding the filter to the browser

This usually means you will need to copy the content from the css file into
your browser's custom.css file or somehow importing the custom CSS file into
your browser. For instance, for `qutebrowser` this file is defined in your
config.py with the setting c.content.user_stylesheets (which can be used to
import the CSS from dontreadthecomments directly, as long as you have a local
copy of the file).
