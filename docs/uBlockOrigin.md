# Using dontreadthecomments filters with uBlock origin

## 1 - Make sure you have uBlock origin

Get it for your browser:

- [Firefox](https://addons.mozilla.org/addon/ublock-origin/)
- [Chromium/Google Chrome](https://chrome.google.com/webstore/detail/ublock-origin/cjpalhdlnbpafiamejdnhcphjbkeiagm)

## 2 - Choose your filter

[Select a filter](filters.md) from the filter list and copy the "Adblock"
filter link. You may wish to try the "Add to uBlock Origin, Adblock Plus and
similar adblockers" link before proceeding to the next step, if that link works
you do not need to follow step 3.

## 3 - Adding the filter

First open the uBlock origin menu by clicking the uBlock origin icon, pictured
below.

![uBlock origin icon](img/ublock-btn.png)

Click the dashboard icon. It is underneath the very big "Power button" icon,
all the way to the right, and looks like three sets of sliders. Hilighted in
red on the image below.

![uBlock origin popup](img/ublock-popup.png)

Make sure you are on the "filter lists" tab.

![uBlock origin menu bar](img/ublock-topmenu.png)

Scroll to the the bottom, and then, under "Custom", click on "Import". Paste
the URL that you copied from the filter list in the second step.

![uBlock custom entry](img/ublock-entry.png)

Then click "Apply changes"

![uBlock apply changes button](img/ublock-apply.png)
